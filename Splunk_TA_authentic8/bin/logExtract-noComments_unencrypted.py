#!/usr/bin/env python

import base64
import datetime
from getopt import getopt # Old school to support Python < 2.7
import json
import os
#import seccure
import sys
import time
import urllib2
from ConfigParser import SafeConfigParser


def usage_abort( extra='' ):
    sys.exit( extra + '''
Usage: python logExtract.py -o <org name> [ -e [ qa | eng | prod ] [ -t <auth token file> ] 
                          [ -i <start id> | -d <start date> ] [ -s <end date> ] 
                          [ -l ] [ --epoch ] [-c] [--no_enc] [--type=<log type>] [ -p [encryption key file] ]

-e specifies the environment, defaults to qa
-t is optional if auth_token file path specified in extapi,ini
-l used for limit, defaults to 1000 log entries when not specified
-i or -d can be used for starting sequence/time for log extraction in the org, defaults to earliest log if not specified
-c used to pull continuously until it reaches the end of logs
--epoch leaves timestamps in epoch format
--no_enc skips the encrypted data when printing logs, leaving only the decrypted data
--type allows you to only filter for a specific type of log (ie, ADMIN_AUDIT, URL, A8SS), works with normal/decrypted logs
-p is used to pull encrypted logs and attempts to decrypt them using key file specified in extapi.ini or optionally a custom path
Example:
python enc_log_extract.py -o "Customer org" -t auth.txt -p pass.txt -d "2014-04-01 14:00:00"''' )

parser = SafeConfigParser()        
parg = 'p'
pval = ''
for i, opt in enumerate(sys.argv):
    if '-p' in opt:
        pval = i
        break
if (i + 1) == len(sys.argv) or not '-' in sys.argv[i+1]:
    parg += ':'
opt_array, args = getopt( sys.argv[1:], 'e:d:i:l:o:t:s:c' + parg, ['epoch' ,'no_enc','type='] )
if args != []:
    usage_abort( ' '.join( args ) + ' would be ignored' )
opts = dict( opt_array )

if opts.get('-e','') is not '':
    env = "-" + opts.get( '-e', '' ) + "."  
else:
    if os.path.isfile('extapi.ini'):
        parser.read('extapi.ini')
        env = "-" + parser.get('global','env') + "."
    else:
        print "Warning, cannot find extapi.ini in folder"
env = env.lower()

if "pro" in env:
    env = "."
elif env is None or (not env in "-qa." and not env in "-eng."):
    print "Could not find valid env", env, "defaulting to -qa."
    env = "-qa."
ea_host = 'extapi' + env + 'authentic8.com'
#print ea_host

if '-o' in opts:
    org = opts['-o']
else:
    usage_abort( 'Missing org' )

token = None    
if "-t" in opts:
    if not os.path.isfile(opts['-t']):
        print 'Warning, ' + opts['-t'] + ' is not a valid file, attempting to use ini file'
    else:
        token = opts['-t']    
if token is None:
    if os.path.isfile('extapi.ini'):
        parser.read('extapi.ini')
        token = parser.get('extractlog', 'auth_token')
        if not os.path.isfile(token):
            usage_abort('Invalid path in extapi.ini for auth token')      
    else:
        usage_abort('Could not find extapi.ini in folder and no valid auth token file found')

decrypt = False
key_file = None

if "-p" in opts and parg == 'p:':
    if not os.path.isfile(opts['-p']):
        print 'Warning, ' + opts['-p'] + ' is not a valid file, attempting to use ini file'
    else:
        key_file = opts['-p']
        decrypt = True
if key_file is None and '-p' in opts and parg == 'p':
    if os.path.isfile('extapi.ini'):
        parser.read('extapi.ini')
        key_file = parser.get('extractlog', 'encrypt_key')
        if not os.path.isfile(key_file):
            print 'Warning, invalid path in extapi.ini for encrypt key, will not be able to decrypt logs'
        else:
            decrypt = True  
    else:
        usage_abort('Could not find extapi.ini in folder and no valid encrypt key file found, will not be able to decrypt logs')
    
t = open( token, 'rb' )
auth_cmd = {
    'command': 'setauth',
    'data': t.read() }
t.close() 

encryptedlogs = ''
if decrypt:
    p = open( key_file )
    passphrase = p.read().rstrip()
    p.close()   
    encryptedlogs += 'from encrypted logs'

ltype = None
if '--type' in opts:
    ltype = opts['--type']

start = 'beginning'
continuous = False
end = ''
if not '-p' in opts:
    if ltype is not None:
        log = ltype
    else:
        log = 'URL'
        ltype = log    
else:
    log = 'ENC'
    
cmd = {
    'command': 'extractlog',
    'org': org,
    'type': log }

if '-i' in opts:
    cmd['start_seq'] = int( opts['-i'] )
if '-c' in opts: 
    continuous = True
if '-d' in opts:
    try:
        opts['-d'] = float(opts['-d'])
    except ValueError:
        pass
    cmd['start_time'] = opts['-d']
if '-s' in opts:
    try:
        opts['-s'] = float(opts['-s'])
    except ValueError:
        pass
    cmd['end_time'] = opts['-s']    
    end = opts['-s']
if '-l' in opts:
    cmd['limit'] = int( opts['-l'] )
if not ( 'start_seq' in cmd or 'start_time' in cmd ):
    cmd['start_seq'] = 0

if ltype == 'ENC':
    decrypt = False

limit = cmd['limit'] if '-l' in opts else 1000
log_count = 0
decrypted_count = 0
calls = 0
total = 0
is_more = True
t0 = time.time()
decrypted = []

if ltype is not None:
    log = ltype
#print cmd
#print "Getting", log, "logs", encryptedlogs

counter = 0

while is_more:
    req = urllib2.Request( 'https://' + ea_host + '/api/',
                       json.dumps( [ auth_cmd, cmd ] ),
                       { 'Content-Type': 'application/json' } )
    #print 'making request', json.dumps([auth_cmd, cmd])
    reader = urllib2.urlopen( req )
    res = json.loads( reader.read() )
    reader.close()
    counter = counter + 1
    #print "request complete", counter
    total = time.time() - t0  
    if 'result' in res[1]:
        for l in res[1]['result']['logs']:
            if not '--epoch' in opts:
                l['create_ts'] = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(l['create_ts']))
            if decrypt:        
                try:
                    l['decrypted'] = json.loads(
                     seccure.decrypt( base64.b64decode( l['enc'] ),
                                    passphrase, curve='secp256r1/nistp256' ) )
                    if '--no_enc' in opts:
                        del(l['enc'])
                    if ltype is not None: 
                        if l['decrypted']['type'] == ltype:
                            decrypted.append(l)
                    else:
                        decrypted.append(l)
			#['decrypted']
			#print "test8"
                except seccure.IntegrityError:
                    print "Could not decrypt log created on", l['create_ts'], "with Key name", l['key_name'] 
        if decrypt:    
            # print 'done'        
            # print json.dumps(decrypted, indent=2, ensure_ascii=False ).encode('utf8')
	    print json.dumps(decrypted)
            for event in decrypted:	
	    	     final=event["decrypted"]		
	    	     final["create_ts"]=event["create_ts"]
	      	     try:
	 	            final["app"]=final["action"]
	             except:
				 pass
	          		 print json.dumps(final,indent=2, ensure_ascii=False).encode('utf8')
		   		 #print "test1"
        else:
	    	final=res[1]['result']['logs']
	    #final["create_ts"]=event["create_ts"]
	        try:
        	        final["app"]=final["action"]
                except:
                	 pass
	          	 print json.dumps(final, indent=2, ensure_ascii=False).encode('utf8')
        is_more = res[1]['result']['is_more']
        #print "is_more:", is_more
        #print "next_seq:", res[1]['result']['next_seq']
        cmd['start_seq'] = res[1]['result']['next_seq']
        log_count = log_count + len(res[1]['result']['logs'])
        calls = calls + 1
        filtered = ''
        decrypted_count += len(decrypted)
        if not continuous:
            break
        #print 'Grabbed', log_count, 'logs. Elapsed time is', total
    else:
        print 'Failure'
        import pprint
        pprint.pprint( res[1]['error'] )
        break
#print '--------------------Summary--------------------'
#print 'Grabbed', log_count, 'log entries from',str(opts['-o']),'org tree'
#if decrypt:
#    print 'Decrypted', decrypted_count, 'logs.'    
#print 'Total time to pull logs is', total, 's'
#print 'Max number of logs from request is', limit
#print 'Logs entries starting from', start
#if len(end) > 0:
#    print 'and ending at', end
#if not continuous:
#  if res[1]['result']['is_more']:
#      print 'More logs remain after current logs, continue from log id', res[1]['result']['next_seq']
#  else:
#      print 'No more logs remain after current logs'
#else:
#  print 'Total time to pull logs is', total, 'in', calls, 'calls'
