#!/bin/bash

submanual(){
while :
do
echo "sub Menu:"
echo -e "\t(x) Options 1"
echo -e "\t(y) Options 1"
echo -e "\t(e) Back"
echo -n "Please enter your choice:"
read c
case $c in
    "x"|"X")
    # Options 1 and its commands
    ;;
    "y"|"Y")
    # Options 2 and its commands
    ;;
    "e"|"E")
    break
    ;;
        *)
        echo "invalid answer, please try again"
        ;;
esac
done
}

submanual2(){
private_encrypted="/opt/splunk/etc/apps/Splunk_TA_authentic8/private.txt"
clear
while :
do
echo "Is the Authentica8 Data you are trying to ingest into splunk encrypted or unencrypted?"
echo -e "\t(1) Encrypted"
echo -e "\t(2) Unencrypted"
echo -e "\t(3) Back"
echo -e "\t(4) Exit"
echo -n "Please enter your choice: "
read choicenow_install
clear
case $choicenow_install in

 	"1")
	path_script=/opt/splunk/etc/apps/Splunk_TA_authentic8/bin
	read -p "What is the name of the organization for your Authentic8 Environment? " org_encrypted
	clear
	read -p "What is the environment that Authentic8 is in (e.g prod, eng. test....)? " authentic8_env
	clear
	echo -e "What interval would you like to set it to pull in terms of timespan? "
  echo -e "\t(1) 5 min ago"
  echo -e "\t(2) 10 min ago"
  echo -e "\t(3) 30 min ago"
	echo -e "\t(4) 60 min ago"
  echo -e "\t(5) 24 hour ago"
  echo -n "Please enter your choice: "
	read date_back_1
		case $date_back_1 in
		"1") date_back_2="5 min ago"  ;;
		"2") date_back_2="10 min ago" ;;
		"3") date_back_2="30 min ago"  ;;
		"4") date_back_2="60 min ago"  ;;
	       	#"5") date_back_2="24 hour ago" ;;
		"6") date_back_2="12 day ago" ;;
		 *) echo "Invalid option....bring you back to the main menu"
		    submanual2 ;;
	esac
	clear
	read -p "What do you want to name your log for the authentic8 data to be sent to (Dont put path, just name and not extension)? " path_place_file

	clear
	read -p "What the private key password? " private_key_1
	clear
	read -p "What is the token file name? " token_encrypted_1
	clear
	echo "Moving things around for you, after the script is done, your ghost.sh file is created. In order to start grabbing the data from authentic8, you would need to turn on the inputs.conf for it to start grabbing data from $path_place_file.log in /opt/splunk/etc/apps/Splunk_TA_authentic8/local/inputs.conf. You would need to change from 1 to 0 under disabled option for the monitor input"
	sleep 2
	clear
	echo "path_script=\"/opt/splunk/etc/apps/Splunk_TA_authentic8/bin\"" > $path_script/ghost.sh
	echo "$private_key_1" > $path_script/private.txt
	echo "private_key=$path_script/private.txt" >> $path_script/ghost.sh
	echo "decrypt=\"true\"" >> $path_script/ghost.sh
	echo "token_encrypted=$path_script/$token_encrypted_1" >> $path_script/ghost.sh
	echo "date_back=\$(date -d \"$date_back_2\" \"+%Y-%m-%d %H:%M:%S\")" >> $path_script/ghost.sh
	echo "date_present=\$(date \"+%Y-%m-%d %H:%M:%S\")" >> $path_script/ghost.sh
	echo "if [ \$decrypt = false ];then

                echo \"If you are seeing this message. Your decryption is not working. Contact Guidepoint Security for further instructions.\"

        else


              if [ ! -f \$private_key ];then

                  echo \"No Private Key does not exist\"

              else

             	    sudo -u splunk touch $path_script/$path_place_file.log
		              python $path_script/logExtract-noComments.py -e $authentic8_env -o \"$org_encrypted\" -t \$token_encrypted -d \"\$date_back\" -s \"\$date_present\" -p \$private_key --no_enc >> $path_script/$path_place_file.log

              fi

        fi"  >> $path_script/ghost.sh

chmod +x $path_script/ghost.sh
chown splunk:splunk $path_script/*
date_back_6=`echo "$date_back_2" | awk '{print $1}'`
echo "*/$date_back_6 * * * * splunk $path_script/ghost.sh >> $path_script/$path_place_file.log" > /etc/cron.d/authentic8
sleep 2
crontab /etc/cron.d/authentic8
        break
	;;

	"2")
	clear
	path_script=/opt/splunk/etc/apps/Splunk_TA_authentic8/bin
	read -p "What is the name of the organization for your Authentic8 Environment? " org_unencrypted
	clear
	read -p "What is the environment that Authentic8 is in (e.g prod, eng. test....)? " authentic8_env
	clear
	echo -e "What interval would you like to set it to pull in terms of timespan? "
	echo -e "\t(1) 5 min ago"
        echo -e "\t(2) 10 min ago"
        echo -e "\t(3) 30 min ago"
	echo -e "\t(4) 60 min ago"
	echo -e "\t(5) 24 hour ago"
        echo -n "Please enter your choice: "
	read date_back_3
		case $date_back_3 in
		"1") date_back_4="5 min ago"  ;;
		"2") date_back_4="10 min ago" ;;
		"3") date_back_4="30 min ago"  ;;
		"4") date_back_4="60 min ago"  ;;
		"5") date_back_4="24 hour ago" ;;
		"6") date_back_4="12 day ago" ;;
		 *) echo "Invalid option....bring you back to the main menu"
		    submanual2 ;;
	esac
	clear
	read -p "What is the token file name? " token_unencrypted_1
	clear
	echo "Moving things around for you, after the script is done, your $path_script/ghost_plain.sh file is created. In order to start grabbing the data from authentic8, you would need to turn on the inputs.conf scripted input for it to start grabbing data. The inputs.conf is located /opt/splunk/etc/apps/Splunk_TA_authentic8/local/inputs.conf."
	sleep 5
	clear
	echo "path_script=\"/opt/splunk/etc/apps/Splunk_TA_authentic8/bin\"" > $path_script/ghost_plain.sh
	echo "decrypt=\"false\"" >> $path_script/ghost_plain.sh
	echo "token_unencrypted=$path_script/$token_unencrypted_1" >> $path_script/ghost_plain.sh
	echo "date_back=\$(date -d \"$date_back_4\" \"+%Y-%m-%d %H:%M:%S\")" >> $path_script/ghost_plain.sh
	echo "date_present=\$(date \"+%Y-%m-%d %H:%M:%S\")" >> $path_script/ghost_plain.sh
	echo "if [ \$decrypt = false ];then

        python $path_script/logExtract-noComments_unencrypted.py -e $authentic8_env -o $org_unencrypted -t \$token_unencrypted -d \"\$date_back\" -s \"\$date_present\"

else

		echo \"There is a problem with the script or you have decrypt turned on.\"

        fi"  >> $path_script/ghost_plain.sh

chmod +x $path_script/ghost_plain.sh
chown splunk:splunk $path_script/*
/opt/splunk/bin/splunk restart
clear
        break
	;;

	"3")
		break;;

	"4") exit 0 ;;
esac
done
}

echo "Checking if the script is running as root......."
sleep 2

if [ $(id -u) != "0" ]; then
        echo "Script must be run as user: root"
        exit 1

else
        echo "Are you sure you want this server to be the collection for all the authentic8 data to be sent (Heavy Forwarder) or store (Single Instance Splunk)? "
        echo -e "\t[Yes]"
        echo -e "\t[No]"
        echo -n "Please enter your choice: "
        read answer
        case $answer in


        "Yes"|"yes")
          clear
          while :
          do
	           echo "Welcome to the installation setup for Authentic8 Technical Add-On App! Please make sure your token file and private key are put into /opt/splunk/etc/apps/Splunk_TA_Authentic8. Choose from the following Menu."
	           echo -e "\t(1) First Time Install"
	           echo -e "\t(2) Quit the Setup"
	           echo -n "Please enter your choice: "
	           read choice
		            case $choice in
    			           "1")
    				             submanual2
    				              ;;
    			           "2")
    				             break
                          ;;
        		           *)
        			            echo "invalid answer, please try again"
                          clear
        			            ;;
		            esac
	        done
          ;;
        "no"|"No")
          echo "Please make sure you run the setmeup script at the place you want to collect data to forward or collect data to store!!!!!!(Heavy Forwarder or Single Instane)"
          sleep 3
          clear
          ;;
        esac
fi
